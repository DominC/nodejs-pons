import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';

export interface AuthResponseData {
  userId: string;
  message: string;
}

@Injectable({ providedIn: 'root' })
export class AuthService {

  constructor(private http: HttpClient, private router: Router) {}

  signup(username: string, password: string, email: string) {
    return this.http
      .post<AuthResponseData>(
        'http://localhost:8080/users',
        {
          username: username,
          email: email,
          password: password
        }
      )
  }

  login(username: string, password: string) {
    return this.http
      .post<AuthResponseData>(
        'http://localhost:8080/login',
        {
          username: username,
          password: password
        }
      )
  }


}
