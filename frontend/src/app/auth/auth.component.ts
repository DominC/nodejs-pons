import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService, AuthResponseData } from './auth.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html'
})
export class AuthComponent {
    isLoginMode = true;
    isLoading = false;
    error: string = null;

    constructor(private authService: AuthService, private router: Router) { }

    onSwitchMode() {
        this.isLoginMode = !this.isLoginMode;
    }

    onSubmit(form: NgForm) {
        if (!form.valid) {
            return;
        }
        const email = form.value.email;
        const password = form.value.password;
        const username = form.value.username;

        this.isLoading = true;

        if (this.isLoginMode) {
            this.authService.login(username, password).subscribe(
                resData => {
                    console.log(resData);
                },
                error => {
                    console.log(error)
                })
        } else {
            this.authService.signup(username, password, email).subscribe(
                resData => {
                    console.log(resData);
                },
                error => {
                    console.log(error)
                }
            );
        }
        form.reset();
    }
}
