const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const authRouter = require('./router/auth')
const flashcardRouter = require('./router/flashcard')
const errorHandler = require('./util/errorHandling')
const corsHandler = require('./util/cors')

const app = express();

app.use(bodyParser.json())
app.use(corsHandler)

app.use(authRouter)
app.use(flashcardRouter)

app.use(errorHandler)

mongoose
    .connect('mongodb://localhost:27017/pons?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false')
    .then(result => {
        app.listen(8080, () => {console.log("Server running on port 8080...")})
    }
    ).catch(err => {
        console.log(err)
    })