const express = require('express')
const { body } = require('express-validator/check')

const authController = require('./../controllers/auth')
const User = require('./../models/user')

const router = express.Router()

router.post('/users', [
    body('email')
        .isEmail()
        .withMessage('Entered email is invalid')
        .custom(async (value, { req }) => {
            const doc = await User.findOne({ email: value })
            if (doc) {
                return Promise.reject("Email already taken")
            }
        })
        .normalizeEmail(),
    body('username')
        .trim()
        .not()
        .isEmpty()
        .custom(async (value, { req }) => {
            const doc = await User.findOne({ username: value })
            if (doc) {
                return Promise.reject("Username already taken")
            }
        }),
    body('password')
        .trim()
        .isLength( { min: 5 })
    ], authController.signup)

router.post('/login', authController.login)

module.exports = router