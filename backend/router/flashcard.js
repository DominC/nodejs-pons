const express = require('express');

const flashcardController = require('./../controllers/flashcard') 
const auth = require('./../util/authorization')

const router = express.Router();

router.get('/translation', auth, flashcardController.findTranslation)

router.post('/flashcards', auth,  flashcardController.createFlashcard)

router.get('/flashcards', auth, flashcardController.getFlashcards)

router.patch('/flashcards/:id', auth, flashcardController.editFlashcard)

module.exports = router