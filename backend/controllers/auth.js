const { validationResult } = require('express-validator/check')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const User = require('./../models/user')

exports.signup = (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        const error = new Error('Validation error');
        error.statusCode = 422;
        error.data = errors.array();
        throw error
    }

    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;

    bcrypt
        .hash(password, 12)
        .then(hashedPasswd => {
            const user = new User({
                email: email,
                username: username,
                password: hashedPasswd
            })
            return user.save()
        }).then(result => {
            res.status(201).json({
                message: 'Created',
                userId: result._id
            })
        }).catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })

}

exports.login = (req, res, next) => {
    const login = req.body.username;
    const password = req.body.password;
    let loadedUser;

    User.findOne({ username: login })
        .then(user => {
            if (!user) {
                const error = new Error('User does not exist');
                error.statusCode = 401;
                throw error
            }
            loadedUser = user
            return bcrypt.compare(password, user.password)
        }).then(isEqual => {
            if (!isEqual) {
                const error = new Error('Incorrect password');
                error.statusCode = 401;
                throw error
            }
            const token = jwt.sign({
                userId: loadedUser._id.toString(),
                username: loadedUser.username,
                email: loadedUser.email
            }, 'ThisIsSuperSecredSecredThatOnlyIKnow', { expiresIn: '1h' })

            res.status(200).json({
                token: token,
                userId: loadedUser._id
            })

        }).catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}

