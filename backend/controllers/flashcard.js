const express = require('express');
const rp = require('request-promise-native');

const Flashcard = require('./../models/flashcard')
const User = require('./../models/user')


exports.findTranslation = (req, res, next) => {
    rp({
        uri: 'https://api.pons.com/v1/dictionary',
        headers: {
            'X-Secret': '471624bf762a2f264c7e5898955badfe1da3201b5f8642b58bfa4e6bf954188f'
        },
        qs: {
            q: req.query.q,
            in: req.query.in,
            l: req.query.to + 'pl'
        },
        json: true
    })
        .then(parsedBody => {
            res.send(parsedBody);
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
}

exports.createFlashcard = (req, res, next) => {
    const userId = req.userId;
    const word = req.body.word;
    const translations = req.body.translations;
    const language = req.body.language
    let savedFlashcard;

    const flashcard = new Flashcard({
        language: language,
        word: word,
        user: userId,
        translations: translations
    });

    flashcard.save()
        .then(result => {
            savedFlashcard = result;
            return User.findById(userId);
        }).then(user => {
            user.flashcards.push(flashcard);
            return user.save();
        }).then(result => {
            res.status(201).json({ savedFlashcard });
        }).catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
}

exports.getFlashcards = (req, res, next) => {
    const userId = req.userId;

    User.findById(userId)
        .populate('flashcards')
        .then(user => {
            res.status(200).json(user.flashcards);
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })

}

exports.editFlashcard = (req, res, next) => {
    const userId = req.userId;
    const flashcardId = req.params.id;
    const newTranslations = req.body.translations

    Flashcard.findById(flashcardId)
        .then(flashcard => {
            if (flashcard.user.toString() !== userId) {
                const error = new Error('Not authorized');
                error.statusCode = 403;np
                throw error;
            }
            flashcard.translations = newTranslations;
            return flashcard.save()
        }).then(result => {
            res.status(200).json(result)
        }).catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}