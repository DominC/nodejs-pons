const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    const authHeader = req.get('Authorization');
    if (!authHeader){
        const error = new Error('No authorization header included')
        error.statusCode = 401;
        throw error
    }
    const token = req.get('Authorization').split(' ')[1];
    let decodetToken;
    try {
        decodetToken = jwt.verify(token, 'ThisIsSuperSecredSecredThatOnlyIKnow')
    } catch (err) {
        err.statusCode = 500
        throw err
    }

    if (!decodetToken) {
        const error = new Error('Not Authorized')
        error.statusCode = 403;
        throw error
    }

    req.userId = decodetToken.userId;
    req.username = decodetToken.username;
    next();

}