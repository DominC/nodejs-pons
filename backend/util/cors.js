module.exports = (req, res, next) => {
    res.setHeader('Access-Controll-Allow-Origin', '*');
    res.setHeader('Access-Controll-Allow-Methods', 'POST, PUT, DELETE, PATCH, GET');
    res.setHeader('Access-Controll-Allow-Headers', '*');
    next()
}