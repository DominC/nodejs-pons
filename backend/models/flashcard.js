const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const flashcardSchema = new Schema({
    language: {
        type: String,
        required: true
    },
    word: {
        type: String,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    translations: [
        {
            translation: {
                type: String,
                required: true
            },
        }
    ]

});

module.exports = mongoose.model('Flashcard', flashcardSchema)